<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\SendOtpCodeMail;
use Mail;

class sendEmailOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if($event->condition == 'register'){
            $pesan = "we're excited to have you get started, first, you need to confirm your account. this is your OTP :";
        }elseif($event->condition == 'regenerate'){
            $pesan = "Regenerate OTP successsfull. this is your OTP code :";
        }

        Mail::to($event->user)->send(new SendOtpCodeMail($event->user, $pesan));
    }
}
