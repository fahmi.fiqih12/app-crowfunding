<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                'id' => str::uuid(),
                'name' => 'admin',
            ],
            [
                'id' => str::uuid(),
                'name' => 'user',
            ]

        ]);
    }
}
