<?php

Route::group([
    'middleware' => 'api',
    'prefix'     => 'auth',
    'namespace'  => 'Auth'

], function() {
    Route::post('register','RegisterController');
    Route::post('regenerate-otp', 'RegenerateOtpCodeController');
    Route::post('verification', 'VerificationController');
    Route::post('update-password', 'updatePasswordController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController')->middleware('auth:api');
    Route::post('check-token', 'CheckTokenController')->middleware('auth:api');
    
    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');

    
}); 

Route::group([
    'middleware' => ['api', 'email_verified', 'auth:api'],

], function(){
    Route::get('profile/show', 'ProfileController@show');
    Route::post('profile/update', 'ProfileController@update');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'campaigns',
],function (){
    Route::get('random/{count}', 'CampaignsController@random');
    Route::post('store', 'CampaignsController@store');
    Route::get('/', 'CampaignsController@index');
    Route::get('/{id}', 'CampaignsController@detail');
    Route::get('/search/{keyword}', 'CampaignsController@search');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'blogs',
],function (){
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});
