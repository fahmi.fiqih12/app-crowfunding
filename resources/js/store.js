import Vue from 'vue'
import Vuex from 'vuex'
import transaction from './store/transactions.js'
import alert from './store/alert.js'
import dialog from './store/dialog.js'
import auth from './store/auth.js'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
    key : 'myApp',
    storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
     plugins: [vuexPersist.plugin],  
     modules:{
         transaction,
         alert,
         auth,
         dialog,
     }
})