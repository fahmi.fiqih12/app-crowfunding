<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class OtpCode extends Model
{
    use Uuids;
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
