<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Events\UserRegistered;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => ['required','string'],
            'email' => ['email', 'required', 'unique:users','email'],
        ]); 

        $data_request = $request->all();

        $user = User::create($data_request);

        event(new UserRegistered($user, 'register'));

        $data['user'] = $user;


        return response()->json([
            'response_code' => '00',
            'response_message' => 'User baru berhasil didaftarkan, Silakan cek email untuk melihat otp',
            'data' => $data
        ]);
    }
}
