<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegisterResourece extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => $this->user->name,
            // 'user' => $this->user->email,        
            // 'user' => $this->user->created_at,        
            // 'user' => $this->user->updated_at,  
        ];
    }
}
